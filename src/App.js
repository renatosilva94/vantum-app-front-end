import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import ListarCliente from "./Pages/ListarCliente";
import CadastroCliente from "./Pages/CadastroCliente";
import EditarCliente from "./Pages/EditarCliente";

import "bootstrap/dist/css/bootstrap.css";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={ListarCliente} />
        <Route path="/CadastroCliente" component={CadastroCliente} />
        <Route path="/EditarCliente/:id" component={EditarCliente} />
      </Switch>
    </Router>
  );
};

export default App;
