import React, { Component } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import { Navbar, Nav, Table, Button } from "react-bootstrap";
import { BrowserRouter as Link } from "react-router-dom";
import Swal from "sweetalert2";

class ListarCliente extends Component {
  state = {
    data: [],
    id: 0,
    nome: null,
    email: null,
    telefone: null,
    endereco: null,
    intervalIsSet: false,
    idToDelete: null,
    idToUpdate: null,
    objectToUpdate: null,
  };

  componentDidMount() {
    this.getDataFromDb();
    if (!this.state.intervalIsSet) {
      let interval = setInterval(this.getDataFromDb, 1000);
      this.setState({ intervalIsSet: interval });
    }
  }

  componentWillUnmount() {
    if (this.state.intervalIsSet) {
      clearInterval(this.state.intervalIsSet);
      this.setState({ intervalIsSet: null });
    }
  }

  getDataFromDb = () => {
    fetch("http://localhost:3001/GET/user")
      .then((data) => data.json())
      .then((res) => this.setState({ data: res.data }));
  };

  putDataToDB = (nome, email, telefone, endereco) => {
    let currentIds = this.state.data.map((data) => data.id);
    let idToBeAdded = 0;
    while (currentIds.includes(idToBeAdded)) {
      ++idToBeAdded;
    }

    axios.post("http://localhost:3001/POST/user", {
      id: idToBeAdded,
      nome: nome,
      email: email,
      telefone: telefone,
      endereco: endereco,
    });
  };

  deleteFromDB = (idTodelete) => {
    parseInt(idTodelete);
    let objIdToDelete = null;
    this.state.data.forEach((dat) => {
      if (dat.id === idTodelete) {
        objIdToDelete = dat._id;
      }
    });

    axios.delete("http://localhost:3001/DELETE/user", {
      data: {
        id: objIdToDelete,
      },
    });

    this.props.history.push("/");
    const Toast = Swal.mixin({
      toast: true,
      position: "top-end",
      showConfirmButton: false,
      timer: 3000,
    });

    Toast.fire({
      type: "warning",
      title: "Cliente Deletado Com Sucesso",
    });
  };

  updateDB = (
    idToUpdate,
    updateNome,
    updateEmail,
    updateEndereco,
    updateTelefone
  ) => {
    let objIdToUpdate = null;
    parseInt(idToUpdate);
    this.state.data.forEach((dat) => {
      if (dat.id === idToUpdate) {
        objIdToUpdate = dat._id;
      }
    });

    axios.put("http://localhost:3001/PUT/user", {
      id: objIdToUpdate,
      update: {
        nome: updateNome,
        email: updateEmail,
        telefone: updateTelefone,
        endereco: updateEndereco,
      },
    });
  };

  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">
            <img
              src="logo.png"
              width="103"
              height="45"
              className="d-inline-block align-top"
              alt="logo"
            />
          </Navbar.Brand>

          <Nav className="mr-auto">
            <Nav.Link href="/">Listagem de Clientes</Nav.Link>
            <Nav.Link href="/CadastroCliente">Cadastro de Clientes</Nav.Link>
          </Nav>
        </Navbar>

        <Table responsive striped bordered hover variant="dark">
          <thead>
            <tr>
              <th>#</th>
              <th>Nome</th>
              <th>E-Mail</th>
              <th>Telefone</th>
              <th>Endereço</th>
              <th>Ações</th>
            </tr>
          </thead>

          <tbody>
            {this.state.data.map((dat) => {
              return (
                <tr key="dat">
                  <td>{dat.id}</td>
                  <td>{dat.nome}</td>
                  <td>{dat.email}</td>
                  <td>{dat.telefone}</td>
                  <td>{dat.endereco}</td>
                  <td>
                    <Button
                      variant="danger"
                      size="lg"
                      onClick={() => this.deleteFromDB(dat.id)}
                    >
                      Deletar Cliente
                    </Button>
                    &nbsp;&nbsp;
                    <Button variant="warning" size="sm">
                      <Link className="btn" to={`/EditarCliente/${dat.id}`}>
                        Editar Cliente
                      </Link>
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default ListarCliente;
