import React, { Component } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import {
  Navbar,
  Nav,
  InputGroup,
  FormControl,
  Button,
  Form,
  Col,
} from "react-bootstrap";
import Swal from "sweetalert2";

class EditarCliente extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      id: this.props.match.params.id,
      nome: null,
      email: null,
      telefone: null,
      endereco: null,
      intervalIsSet: false,
      idToDelete: null,
      idToUpdate: this.props.match.params.id,
      objectToUpdate: null,
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    console.log(props);
  }

  handleInputChange(e) {
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  componentDidMount() {
    this.getDataFromDb();

    if (!this.state.intervalIsSet) {
      let interval = setInterval(this.getDataFromDb, 1000);
      this.setState({ intervalIsSet: interval });
    }
  }

  componentWillUnmount() {
    if (this.state.intervalIsSet) {
      clearInterval(this.state.intervalIsSet);
      this.setState({ intervalIsSet: null });
    }
  }

  getDataFromDb = () => {
    fetch("http://localhost:3001/GET/user")
      .then((data) => data.json())
      .then((res) => this.setState({ data: res.data }));
  };

  putDataToDB = (nome, email, telefone, endereco) => {
    let currentIds = this.state.data.map((data) => data.id);
    let idToBeAdded = 0;
    while (currentIds.includes(idToBeAdded)) {
      ++idToBeAdded;
    }

    axios.post("http://localhost:3001/POST/user", {
      id: idToBeAdded,
      nome: nome,
      email: email,
      telefone: telefone,
      endereco: endereco,
    });
  };

  deleteFromDB = (idTodelete) => {
    parseInt(idTodelete);
    let objIdToDelete = null;
    this.state.data.forEach((dat) => {
      if (dat.id === idTodelete) {
        objIdToDelete = dat._id;
      }
    });

    axios.delete("http://localhost:3001/DELETE/user", {
      data: {
        id: objIdToDelete,
      },
    });
  };

  updateDB = (
    idToUpdate,
    updateNome,
    updateEmail,
    updateTelefone,
    updateEndereco
  ) => {
    let objIdToUpdate = null;
    parseInt(idToUpdate);
    this.state.data.forEach((dat) => {
      if (dat.id === idToUpdate) {
        objIdToUpdate = dat._id;
      }
    });

    axios
      .put("http://localhost:3001/PUT/user", {
        id: objIdToUpdate,
        update: {
          nome: updateNome,
          email: updateEmail,
          telefone: updateTelefone,
          endereco: updateEndereco,
        },
      })
      .then((response) => {
        this.props.history.push("/");
        const Toast = Swal.mixin({
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
        });

        Toast.fire({
          type: "info",
          title: "Cliente Atualizado Com Sucesso",
        });
      });
  };

  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">
            <img
              src="../logo.png"
              width="103"
              height="45"
              className="d-inline-block align-top"
              alt="logo"
            />
          </Navbar.Brand>

          <Nav className="mr-auto">
            <Nav.Link href="/">Listagem de Clientes</Nav.Link>
            <Nav.Link href="/CadastroCliente">Cadastro de Clientes</Nav.Link>
          </Nav>
        </Navbar>

        <Form>
          <Form.Row>
            <Col>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">*</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  onChange={(e) =>
                    this.setState({ updateNome: e.target.value })
                  }
                  placeholder="Novo Nome"
                />
              </InputGroup>

              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  onChange={(e) =>
                    this.setState({ updateEmail: e.target.value })
                  }
                  placeholder="Novo E-Mail"
                />
              </InputGroup>
            </Col>

            <Col>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">(XX)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  onChange={(e) =>
                    this.setState({ updateTelefone: e.target.value })
                  }
                  placeholder="Novo Telefone"
                />
              </InputGroup>

              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">!</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  onChange={(e) =>
                    this.setState({ updateEndereco: e.target.value })
                  }
                  placeholder="Novo Endereço"
                />
              </InputGroup>
            </Col>

            <Button
              block
              variant="info"
              onClick={() =>
                this.updateDB(
                  this.state.idToUpdate,
                  this.state.updateNome,
                  this.state.updateEmail,
                  this.state.updateTelefone,
                  this.state.updateEndereco
                )
              }
            >
              Atualizar Cliente
            </Button>
          </Form.Row>
        </Form>
      </div>
    );
  }
}

export default EditarCliente;
