import React, { Component } from "react";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import {
  Navbar,
  Nav,
  InputGroup,
  FormControl,
  Button,
  Form,
  Col,
} from "react-bootstrap";
import Swal from "sweetalert2";

class CadastroCliente extends Component {
  state = {
    data: [],
    id: 0,
    nome: null,
    email: null,
    telefone: null,
    endereco: null,
    intervalIsSet: false,
    idToDelete: null,
    idToUpdate: null,
    objectToUpdate: null,
  };

  componentDidMount() {
    this.getDataFromDb();
    if (!this.state.intervalIsSet) {
      let interval = setInterval(this.getDataFromDb, 1000);
      this.setState({ intervalIsSet: interval });
    }
  }

  componentWillUnmount() {
    if (this.state.intervalIsSet) {
      clearInterval(this.state.intervalIsSet);
      this.setState({ intervalIsSet: null });
    }
  }

  getDataFromDb = () => {
    fetch("http://localhost:3001/GET/user")
      .then((data) => data.json())
      .then((res) => this.setState({ data: res.data }));
  };

  putDataToDB = (nome, email, telefone, endereco) => {
    let currentIds = this.state.data.map((data) => data.id);
    let idToBeAdded = 0;
    let re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/gim;

    if (!nome || !email || !telefone || !endereco) {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });

      Toast.fire({
        type: "error",
        title: "Preencha Todos Os Campos",
      });
    } else if (email === "" || !re.test(email)) {
      const Toast = Swal.mixin({
        toast: true,
        position: "top-end",
        showConfirmButton: false,
        timer: 3000,
      });

      Toast.fire({
        type: "error",
        title: "E-Mail Inválido",
      });
    } else {
      while (currentIds.includes(idToBeAdded)) {
        ++idToBeAdded;
      }

      axios
        .post("http://localhost:3001/POST/user", {
          id: idToBeAdded,
          nome: nome,
          email: email,
          telefone: telefone,
          endereco: endereco,
        })
        .then((response) => {
          this.props.history.push("/");

          const Toast = Swal.mixin({
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
          });

          Toast.fire({
            type: "success",
            title: "Cliente Cadastrado Com Sucesso",
          });
        });
    }
  };

  deleteFromDB = (idTodelete) => {
    parseInt(idTodelete);
    let objIdToDelete = null;
    this.state.data.forEach((dat) => {
      if (dat.id === idTodelete) {
        objIdToDelete = dat._id;
      }
    });

    axios.delete("http://localhost:3001/DELETE/user", {
      data: {
        id: objIdToDelete,
      },
    });
  };

  updateDB = (
    idToUpdate,
    updateNome,
    updateEmail,
    updateEndereco,
    updateTelefone
  ) => {
    let objIdToUpdate = null;
    parseInt(idToUpdate);
    this.state.data.forEach((dat) => {
      if (dat.id === idToUpdate) {
        objIdToUpdate = dat._id;
      }
    });

    axios.put("http://localhost:3001/PUT/user", {
      id: objIdToUpdate,
      update: {
        nome: updateNome,
        email: updateEmail,
        telefone: updateTelefone,
        endereco: updateEndereco,
      },
    });
  };

  render() {
    return (
      <div>
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">
            <img
              src="logo.png"
              width="103"
              height="45"
              className="d-inline-block align-top"
              alt="logo"
            />
          </Navbar.Brand>

          <Nav className="mr-auto">
            <Nav.Link href="/">Listagem de Clientes</Nav.Link>
            <Nav.Link href="/CadastroCliente">Cadastro de Clientes</Nav.Link>
          </Nav>
        </Navbar>

        <Form>
          <Form.Row>
            <Col>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">*</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  onChange={(e) => this.setState({ nome: e.target.value })}
                  placeholder="Nome"
                  aria-label="Nome"
                  aria-describedby="basic-addon1"
                />
              </InputGroup>

              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">@</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  onChange={(e) => this.setState({ email: e.target.value })}
                  placeholder="E-Mail"
                  aria-label="E-Mail"
                  aria-describedby="basic-addon1"
                />
              </InputGroup>
            </Col>
            <Col>
              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">(XX)</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  onChange={(e) => this.setState({ telefone: e.target.value })}
                  placeholder="Telefone"
                  aria-label="Telefone"
                  aria-describedby="basic-addon1"
                />
              </InputGroup>

              <InputGroup className="mb-3">
                <InputGroup.Prepend>
                  <InputGroup.Text id="basic-addon1">!</InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  onChange={(e) => this.setState({ endereco: e.target.value })}
                  placeholder="Endereço"
                  aria-label="Endereço"
                  aria-describedby="basic-addon1"
                />
              </InputGroup>
            </Col>
          </Form.Row>
        </Form>

        <Button
          block
          variant="success"
          onClick={() =>
            this.putDataToDB(
              this.state.nome,
              this.state.email,
              this.state.telefone,
              this.state.endereco
            )
          }
        >
          Salvar Cliente
        </Button>
      </div>
    );
  }
}

export default CadastroCliente;
